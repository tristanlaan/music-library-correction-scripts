#!/usr/bin/env python3
# Transcode FLAC files to iTunes compatible formats.
#
# Copyright (C) 2020 Tristan Laan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from glob import iglob
from pathlib import Path
import subprocess
import traceback

directory = {
    'alac': 'Apple Lossless',
    'aac': 'AAC'
}

ffmpeg_settings = {
    'alac': ['-acodec', 'alac', '-vn'],
    'aac': ['-acodec', 'aac', '-b:a', '160k', '-vn']
}

optimized_images = set()


def set_thumbnail(file, dir, extension='jpg'):
    image = next(iglob(f"{dir}/*.{extension}"), None)

    if image is None:
        if extension == 'jpg':
            set_thumbnail(file, dir, extension='jpeg')
        elif extension == 'jpeg':
            set_thumbnail(file, dir, extension='png')
        return

    if image not in optimized_images:
        try:
            subprocess.run(['jpegoptim', image])
            optimized_images.add(image)
        except:
            traceback.print_exc()

    try:
        subprocess.run(['AtomicParsley', str(file), '--artwork', image,
                        '--overWrite'])
    except:
        traceback.print_exc()


def transcode_file(ffmpeg_binary, file, type):
    original_dir = Path(file).parent
    dir = original_dir / f'{directory[type]}'

    if not dir.exists():
        dir.mkdir()

    new_file = dir / f"{Path(file).stem}.m4a"
    print(new_file)

    if not new_file.exists():
        subprocess.run([ffmpeg_binary, '-i', file, *ffmpeg_settings[type],
                        str(new_file)])
        set_thumbnail(new_file, original_dir)


if __name__ == "__main__":
    path = "/media/laannas-gedeeld/Fleedwood Mac"
    ffmpeg_binary = "ffmpeg"

    for file in iglob(f"{path}/**/*.flac", recursive=True):
        if "surround" in file.lower() or "quadraphonic" in file.lower():
            continue
        # print(file)
        transcode_file(ffmpeg_binary, file, 'alac')
        transcode_file(ffmpeg_binary, file, 'aac')
