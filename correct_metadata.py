#!/usr/bin/env python3
# Copies music files to folder structure based on tags.
#
# Copyright (C) 2020 Tristan Laan
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import annotations

from glob import glob, iglob
from pathlib import Path
from random import shuffle
from typing import ClassVar, Dict, Tuple, Optional, List, Union, Any
from mutagen.flac import FLAC
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4
from mutagen import id3
from bisect import insort_left

from pathvalidate import sanitize_filename
import mutagen
import shutil
import subprocess
import traceback

SPECIAL_FOLDERS = {'Surround', 'Quadraphonic', 'Apple Lossless', '7.1-mapping',
                   'AAC'}


class Album:
    albums: ClassVar[Dict[Tuple[str, str, Optional[str]], Album]] = {}

    def __init__(self, artist: str, album: str, special: Optional[str],
                 total_tracks: List[int], total_discs: int,
                 year: Optional[int], songs: List[Song]) -> None:
        if artist == "Various Artists":
            path_artist = 'Compilations'
        else:
            path_artist = artist
        self.album_path = \
            Path(self._sanitize(path_artist)) / self._sanitize(album)

        if special is not None:
            self.album_path /= special
        self.artist = artist
        self.album = album
        self.total_tracks = total_tracks
        self.total_discs = total_discs
        self.year = year
        self.songs = songs

    def add_song(self, song: Song) -> None:
        insort_left(self.songs, song)

    def renew_track_info(self, disc: int, disc_count: int,
                         track_count: int, year: Optional[int]) -> None:
        if disc_count > self.total_discs:
            self.total_tracks.extend([0] * (disc_count - self.total_discs))
            self.total_discs = disc_count
        if track_count > self.total_tracks[disc - 1]:
            self.total_tracks[disc - 1] = track_count
        if year is not None:
            if self.year is None or year > self.year:
                self.year = year

    def set_track_info_songs(self) -> None:
        for song in self.songs:
            song.total_tracks = self.total_tracks[song.disc - 1]
            song.total_discs = self.total_discs


    def move_new_destination(self, origin: Path, destination: Path) -> None:
        path = destination / self.album_path
        path.mkdir(parents=True, exist_ok=True)
        track_len = len(str(max(self.total_tracks)))
        for song in self.songs:
            song.move(path, track_len, song.year)
            song.correct_metadata()

        original_folder = \
            origin / self._sanitize(self.artist) / self._sanitize(self.album)
        if original_folder.exists() and original_folder.is_dir():
            image = self._get_image(original_folder)
            if image is not None:
                image = Path(image)
                shutil.copy2(image, path / image.name)

    def _get_image(self, folder, extension='jpg'):
        image = next(iglob(f"{folder}/*.{extension}"), None)

        if image is None:
            if extension == 'jpg':
                image = self._get_image(folder, extension='jpeg')
            elif extension == 'jpeg':
                image = self._get_image(folder, extension='png')

        return image

    @staticmethod
    def _sanitize(filename: str) -> str:
        return sanitize_filename(filename, platform='universal')

    @staticmethod
    def _get_special(song: Song) -> Optional[str]:
        special_stack = []
        path = song.file

        while True:
            if path.parent.name not in SPECIAL_FOLDERS:
                break

            special_stack.append(path.parent.name)
            path = path.parent

        if len(special_stack) < 1:
            return None

        special_stack.reverse()
        return '/'.join(special_stack)

    @classmethod
    def add_song_albums(cls, song: Song) -> None:
        special = cls._get_special(song)
        disc_count = max(song.total_discs, song.disc)
        track_count = max(song.total_tracks, song.track)
        index = (song.album_artist, song.album, special)
        if index not in cls.albums:
            track_counts = [0] * disc_count
            track_counts[song.disc - 1] = track_count
            cls.albums[index] = cls(song.album_artist, song.album, special,
                                    track_counts, disc_count, song.year,
                                    [song])
        else:
            cls.albums[index].renew_track_info(song.disc, disc_count,
                                               track_count, song.year)
            cls.albums[index].add_song(song)

    def __str__(self):
        return f"{{path: '{self.album_path}', artist: '{self.artist}', " \
               f"album: '{self.album}', year: {self.year}, " \
               f"total discs: {self.total_discs}, " \
               f"total tracks: {self.total_tracks}, songs: {self.songs}}}"

    def info_string(self, song_info=True):
        string = f"path: '{self.album_path}'\nartist: '{self.artist}'\n" \
                 f"album: '{self.album}'\nyear: {self.year}\n" \
                 f"total discs: {self.total_discs}\n"

        for disc, track_count in enumerate(self.total_tracks):
            string += f"total tracks disc {disc}: {track_count}\n"
        string += f"song count: {len(self.songs)}\n"

        if song_info:
            string += "songs:\n"

            for song in self.songs:
                for line in song.info_string().splitlines():
                    string += f"    {line}\n"
                string += '\n'

        return string


class Song:
    def __init__(self, type: str, file: Path, title: str, artist: str,
                 album: str, track: int, disc: int, *,
                 album_artist: Optional[str] = None,
                 year: Optional[int] = None,
                 genre: Optional[Union[str, List[str]]] = None,
                 total_tracks: Optional[int] = None,
                 total_discs: Optional[int] = None) -> None:
        self.type = type
        self.file = file
        self.title = title
        self.artist = artist
        self.album = album
        self.album_artist = album_artist if album_artist is not None \
            else artist
        self.year = year
        self.genre = genre
        self.track = track
        self.total_tracks = total_tracks if total_tracks is not None else track
        self.disc = disc
        self.total_discs = total_discs if total_discs is not None else disc

    def move(self, path: Path, track_len: int, year: int) -> None:
        disc_len = len(str(self.total_discs))
        prefix = f'{self.disc:0={disc_len}}-' if self.total_discs > 1 else ''
        filename = f"{prefix}{self.track:0={track_len}} " \
                   f"{self.title}{self.file.suffix}"
        new_file = path / sanitize_filename(filename, platform='universal')
        shutil.copy2(self.file, new_file)
        self.file = new_file

    def correct_metadata(self) -> None:
        if self.type == 'mp3':
            audio = id3.ID3(self.file)
            audio.add(id3.TPE2(encoding=3, text=self.album_artist))
            audio.add(id3.TRCK(encoding=3, text=f"{self.track}/{self.total_tracks}"))
            audio.add(id3.TPOS(encoding=3, text=f"{self.disc}/{self.total_discs}"))
            audio.delall('TXXX:TOTALTRACKS')
            audio.delall('TXXX:TOTALDISCS')
            audio.save()
        elif self.type == 'm4a':
            tags = MP4(self.file)
            tags['aART'] = self.album_artist
            tags['trkn'] = [(self.track, self.total_tracks)]
            tags['disk'] = [(self.disc, self.total_discs)]
            tags.save()
        elif self.type == 'flac':
            tags = FLAC(self.file)
            tags['ALBUMARTIST'] = self.album_artist
            tags['TRACKNUMBER'] = str(self.track)
            tags['TOTALTRACKS'] = str(self.total_tracks)
            tags['DISCNUMBER'] = str(self.disc)
            tags['TOTALDISCS'] = str(self.total_discs)
            tags.save()

    @staticmethod
    def _get_number_and_total(metadata: mutagen.Tags, number_field: str,
                              total_field: str, list_element: bool = True
                              ) -> Tuple[int, Optional[int]]:
        if list_element:
            split = str(metadata.get(number_field, [1])[0]).split('/')
        else:
            split = str(metadata.get(number_field, 1)).split('/')
        number = int(split[0])
        if len(split) > 1:
            total = split[1]
        else:
            total = None

        if list_element:
            total = metadata.get(total_field, [total])[0]
        else:
            total = metadata.get(total_field, total)
        if total is not None:
            if not list_element:
                total = str(total)
            total = int(total)

        return number, total

    @classmethod
    def parse_from_file(cls, file: Path) -> Song:
        if file.suffix == '.flac':
            metadata = FLAC(file).tags
        elif file.suffix == '.mp3':
            metadata = MP3(file).tags
        elif file.suffix == '.m4a':
            metadata = MP4(file).tags
        else:
            raise NotImplementedError

        if file.suffix == '.flac':
            type = 'flac'
            title = metadata.get('TITLE', ['Unknown Title'])[0]
            artist = metadata.get('ARTIST', ['Unknown Artist'])[0]
            album_artist = metadata.get('ALBUMARTIST', [artist])[0]
            album = metadata.get('ALBUM', ['Unknown Album'])[0]
            year = metadata.get('DATE', [None])[0]
            if year is not None:
                year = int(year)
            genre = metadata.get('GENRE', [None])[0]
            track, total_tracks = cls._get_number_and_total(metadata,
                                                            'TRACKNUMBER',
                                                            'TOTALTRACKS')
            disc, total_discs = cls._get_number_and_total(metadata,
                                                          'DISCNUMBER',
                                                          'TOTALDISCS')
        elif file.suffix == '.m4a':
            type = 'm4a'
            title = metadata.get('©nam', ['Unknown Title'])[0]
            artist = metadata.get('©ART', ['Unknown Artist'])[0]
            album_artist = metadata.get('aART', [artist])[0]
            album = metadata.get('©alb', ['Unknown Album'])[0]
            year = metadata.get('©day', [None])[0]
            if year is not None:
                year = int(year)
            genre = metadata.get('©gen')
            if genre is not None and len(genre) == 1:
                genre = genre[0]
            trkn = metadata.get('trkn', [None])[0]
            if trkn is None:
                track = 1
                total_tracks = None
            else:
                track, total_tracks = [int(t) for t in trkn]
            disk = metadata.get('disk', [None])[0]
            if disk is None:
                disc = 1
                total_discs = None
            else:
                disc, total_discs = [int(d) for d in disk]
        else:
            type = 'mp3'
            title = str(metadata.get('TIT2', 'Unknown Title'))
            artist = str(metadata.get('TPE1', 'Unknown Artist'))
            album_artist = str(metadata.get('TPE2', artist))
            album = str(metadata.get('TALB', 'Unknown Album'))
            year = metadata.get('TDRC')
            if year is not None:
                year = int(str(year))
            genre = metadata.get('TCON')
            if genre is not None:
                genre = str(genre)
            track, total_tracks = cls._get_number_and_total(metadata,
                                                            'TRCK',
                                                            'TXXX:TOTALTRACKS',
                                                            True)
            disc, total_discs = cls._get_number_and_total(metadata,
                                                          'TPOS',
                                                          'TXXX:TOTALDISCS',
                                                          True)

        return cls(type, file, title, artist, album, track, disc,
                   album_artist=album_artist, year=year, genre=genre,
                   total_tracks=total_tracks, total_discs=total_discs)

    def __str__(self):
        string = f"{{type: {self.type}, file: {self.file}, " \
                 f"title: {self.title}, artist: {self.artist}, " \
                 f"album: {self.album}, album artist: {self.album_artist}, " \
                 f"track: {self.track}, total tracks: {self.total_tracks}, " \
                 f"disc: {self.disc}, total discs: {self.total_discs}"
        if self.year is not None:
            string += f", year: {self.year}"
        if self.genre is not None:
            string += f", genre: {self.genre}"
        string += "}"
        return string

    def __eq__(self, other):
        if not isinstance(other, Song):
            return (self.disc, self.track) == other
        return (self.disc, self.track) == (other.disc, other.track)

    def __ne__(self, other):
        if not isinstance(other, Song):
            return (self.disc, self.track) != other
        return (self.disc, self.track) != (other.disc, other.track)

    def __lt__(self, other):
        if not isinstance(other, Song):
            return (self.disc, self.track) < other
        return (self.disc, self.track) < (other.disc, other.track)

    def __le__(self, other):
        if not isinstance(other, Song):
            return (self.disc, self.track) <= other
        return (self.disc, self.track) <= (other.disc, other.track)

    def __gt__(self, other):
        if not isinstance(other, Song):
            return (self.disc, self.track) > other
        return (self.disc, self.track) > (other.disc, other.track)

    def __ge__(self, other):
        if not isinstance(other, Song):
            return (self.disc, self.track) >= other
        return (self.disc, self.track) >= (other.disc, other.track)

    def info_string(self):
        string = f"type: {self.type}\nfile: {self.file}\n" \
                 f"title: {self.title}\nartist: {self.artist}\n" \
                 f"album: {self.album}\nalbum artist: {self.album_artist}\n" \
                 f"track: {self.track}\ntotal tracks: {self.total_tracks}\n" \
                 f"disc: {self.disc}\ntotal discs: {self.total_discs}\n" \
                 f"year: {self.year if self.year is not None else ''}\n" \
                 f"genre: {self.genre if self.genre is not None else ''}\n"
        return string


if __name__ == "__main__":
    from tqdm import trange
    path = "/media/laannas-gedeeld/Fleedwood Mac"
    dest = "/media/laannas-gedeeld/Music"
    ffmpeg_binary = "ffmpeg"

    files = []

    for extension in ['flac', 'm4a', 'mp3']:
        files += glob(f"{path}/**/*.{extension}", recursive=True)

    files = [Path(file) for file in files]

    # for file in files:
    #     metadata = mutagen.File(file)
    #     print(file)
    #     print(metadata.info.pprint())
    #     print(metadata.tags.pprint())
    #     print()

    songs = [Song.parse_from_file(file) for file in files]

    for song in songs:
        Album.add_song_albums(song)

    # for album in Album.albums:
    #     print(album.pprint())

    with trange(len(Album.albums), unit='albums') as t:
        for i, album in zip(t, Album.albums.values()):
            t.set_description(f'{album.artist} - {album.album}')
            album.set_track_info_songs()
            album.move_new_destination(Path(path), Path(dest))
