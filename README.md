# Music library correction scripts
Personal project for creating a standardized folder structure for songs based on
MP3/M4a/FLAC tags.

## Structure
- [`correct_metadata.py`](./correct_metadata.py) \
  Copies music files to folder structure based on tags.
- [`correct_metadata_usb.py`](./correct_metadata_usb.py) \
  Same as `correct_metadata.py`, but also solves some quirks related to the
  audio system of a 2012 car that doesn't understand all tags.
- [`flac.py`](./flac.py) \
  Transcodes FLAC files to iTunes compatible formats. Creates both a lossless
  and lossy copy.

## License
Copyright (C) 2020 Tristan Laan

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see
[https://www.gnu.org/licenses/](https://www.gnu.org/licenses/).
